package com.crazyfriday.flickr.myflickrreader.network


interface ConnectivityCheck {
    fun isConnected(): Boolean
}