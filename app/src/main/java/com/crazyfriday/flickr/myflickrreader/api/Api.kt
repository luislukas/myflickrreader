package com.crazyfriday.flickr.myflickrreader.api

import com.crazyfriday.flickr.myflickrreader.model.FlickrFeedResponse
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface Api {
    @GET("/services/feeds/photos_public.gne?format=json&nojsoncallback=1")
    fun fetchPublicPhotos(): Observable<Response<FlickrFeedResponse>>
}