package com.crazyfriday.flickr.myflickrreader.exception

import java.io.IOException

class NoNetworkException: IOException()