package com.crazyfriday.flickr.myflickrreader.di

import android.content.Context
import android.support.annotation.VisibleForTesting
import com.crazyfriday.flickr.myflickrreader.BuildConfig
import com.crazyfriday.flickr.myflickrreader.api.Api
import com.crazyfriday.flickr.myflickrreader.exception.NoNetworkException
import com.crazyfriday.flickr.myflickrreader.loader.ImageAndMetadataLoader
import com.crazyfriday.flickr.myflickrreader.network.ConnectivityCheck
import com.crazyfriday.flickr.myflickrreader.network.ConnectivityCheckImpl
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module(includes = arrayOf(ResponseResolverModule::class))
open class AppModule(val mContext: Context) {

    private val BASE_URL = BuildConfig.BASE_URL
    private val DEFAULT_TIMEOUT: Int = BuildConfig.DEFAULT_TIMEOUT

    @Provides
    @Singleton
    fun provideContext(): Context {
        return mContext
    }

    @Provides
    @Singleton
    fun provideOKHTTP(connectivityCheck: ConnectivityCheck): OkHttpClient {
        val logInterceptor = HttpLoggingInterceptor()
        logInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

        val networkInterceptor = object : Interceptor {
            override fun intercept(chain: Interceptor.Chain): Response {
                if (connectivityCheck.isConnected()) {
                    return chain.proceed(chain.request())
                } else {
                    throw NoNetworkException()
                }
            }
        }

        return OkHttpClient.Builder()
                .addInterceptor(networkInterceptor)
                .addInterceptor(logInterceptor)
                .connectTimeout(DEFAULT_TIMEOUT.toLong(), TimeUnit.SECONDS)
                .writeTimeout(DEFAULT_TIMEOUT.toLong(), TimeUnit.SECONDS)
                .readTimeout(DEFAULT_TIMEOUT.toLong(), TimeUnit.SECONDS)
                .build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(okhttp: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .baseUrl(getBaseURL())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(okhttp).build()
    }

    @VisibleForTesting
    open fun getBaseURL(): String {
        return BASE_URL
    }

    @Provides
    @Singleton
    fun provideFeedApi(retrofit: Retrofit): Api {
        return retrofit.create(Api::class.java)
    }

    @Provides
    @Singleton
    fun provideConnectivityCheck(): ConnectivityCheck {
        return ConnectivityCheckImpl(mContext)
    }

    @Provides
    @Singleton
    fun provideImageAndMetadataLoaader(okhttp: OkHttpClient): ImageAndMetadataLoader {
        return ImageAndMetadataLoader(okhttp)
    }
}