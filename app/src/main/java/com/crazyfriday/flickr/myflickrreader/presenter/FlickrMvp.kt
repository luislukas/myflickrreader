package com.crazyfriday.flickr.myflickrreader.presenter

import com.crazyfriday.flickr.myflickrreader.model.FlickrFeedResponse

interface FlickrMvp {
    interface Presenter {
        fun onAttachView(view: FlickrMvp.View)
        fun onDetachView()
        fun onDestroy()
        fun fetch(refresh: Boolean)
    }

    interface View {
        fun onSuccess(response: FlickrFeedResponse)
        fun onError()
        fun onNoConnection()
        fun displayProgress(show: Boolean)
        fun dismissSwipeToRefresh()
        fun announceResults(results: Int)
    }
}