package com.crazyfriday.flickr.myflickrreader.presenter

import com.crazyfriday.flickr.myflickrreader.api.ApiResponse
import com.crazyfriday.flickr.myflickrreader.api.FlickrApi
import com.crazyfriday.flickr.myflickrreader.model.FlickrFeedResponse


class FlickrPresenter(private val mFlickrApi: FlickrApi): FlickrMvp.Presenter {
    private var mView: FlickrMvp.View? = null
    private var mResponse: FlickrFeedResponse? = null

    private val mHandle = object : ApiResponse<FlickrFeedResponse> {
        override fun onError() {
            mView?.let {
                it.displayProgress(false)
                it.dismissSwipeToRefresh()
                it.onError()
            }
        }

        override fun onNoConnection() {
            mView?.let {
                it.displayProgress(false)
                it.dismissSwipeToRefresh()
                it.onNoConnection()
            }
        }

        override fun onSuccess(t: FlickrFeedResponse) {
            mResponse = t
            onSuccessResult(t)
        }
    }

    override fun onAttachView(view: FlickrMvp.View) {
        mView = view
        fetch(false)
    }

    override fun onDetachView() {
        mView = null
    }

    override fun onDestroy() {
        mFlickrApi.dispose()
    }

    override fun fetch(refresh: Boolean) {
        mView?.displayProgress(true)
        if (mResponse == null || refresh) {
            mFlickrApi.fetch(mHandle)
        } else {
            mResponse?.let {
                onSuccessResult(it)
            }
        }
    }

    private fun onSuccessResult(response: FlickrFeedResponse) {
        mView?.let {
            it.displayProgress(false)
            it.dismissSwipeToRefresh()
            it.onSuccess(response)
            it.announceResults(response.items.size)
        }
    }
}