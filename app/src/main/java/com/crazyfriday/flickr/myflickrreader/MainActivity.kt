package com.crazyfriday.flickr.myflickrreader

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.Snackbar.LENGTH_LONG
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.view.View
import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityManager
import com.crazyfriday.flickr.myflickrreader.adapter.FlickrAdapter
import com.crazyfriday.flickr.myflickrreader.di.Injector
import com.crazyfriday.flickr.myflickrreader.loader.ImageAndMetadataLoader
import com.crazyfriday.flickr.myflickrreader.model.FlickrFeedResponse
import com.crazyfriday.flickr.myflickrreader.model.FlickrItem
import com.crazyfriday.flickr.myflickrreader.presenter.FlickrMvp
import com.crazyfriday.flickr.myflickrreader.presenter.FlickrPresenter
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class MainActivity : AppCompatActivity(), FlickrMvp.View {

    @Inject
    lateinit var mImageAndMetadataLoader: ImageAndMetadataLoader

    @Inject
    lateinit var mPresenter: FlickrPresenter

    private var mAdapter: FlickrAdapter? = null

    lateinit var mAccessibilityManager: AccessibilityManager

    private val mOnItemClickCallback = object: FlickrAdapter.ImageClickCallback {
        override fun onImageClick(item: FlickrItem) {
            displayImageInBrowser(item.media.m)
        }

        private fun displayImageInBrowser(url: String) {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(url)
            intent.resolveActivity(packageManager)?.let{
                startActivity(intent)
                return
            }
            Snackbar.make(mSwipeToRefesh, getString(R.string.no_available_intent_open_image), LENGTH_LONG).show()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Injector.injectActivity(this)
        mAccessibilityManager = getSystemService(Context.ACCESSIBILITY_SERVICE) as AccessibilityManager
        mAdapter = FlickrAdapter(mutableListOf<FlickrItem>(), mOnItemClickCallback, mImageAndMetadataLoader)
        mRecyclerView.adapter = mAdapter
        mRecyclerView.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        mSwipeToRefesh.setOnRefreshListener {
            mPresenter.fetch(true)
        }
    }

    override fun onResume() {
        super.onResume()
        mPresenter.onAttachView(this)
    }

    override fun onPause() {
        mPresenter.onDetachView()
        super.onPause()
    }

    override fun onDestroy() {
        mPresenter.onDestroy()
        super.onDestroy()
    }

    override fun onSuccess(response: FlickrFeedResponse) {
        mAdapter?.updateList(response.items.asList())
        mErrorContainer.visibility = View.GONE
        mRecyclerView.visibility = View.VISIBLE
    }

    override fun onError() {
        mErrorTextMessage.text = getString(R.string.api_on_error_txt)
        mErrorContainer.visibility = View.VISIBLE
        mRecyclerView.visibility = View.GONE
    }

    override fun onNoConnection() {
        mErrorTextMessage.text = getString(R.string.api_on_no_connection_txt)
        mErrorContainer.visibility = View.VISIBLE
        mRecyclerView.visibility = View.GONE
    }

    override fun displayProgress(show: Boolean) {
        mProgress.visibility = if(show) View.VISIBLE else View.GONE
    }

    override fun dismissSwipeToRefresh() {
        if (mSwipeToRefesh.isRefreshing) {
            mSwipeToRefesh.isRefreshing = false
        }
    }

    override fun announceResults(results: Int) {
        if (mAccessibilityManager.isEnabled) {
            mAccessibilityManager.sendAccessibilityEvent(AccessibilityEvent.obtain(AccessibilityEvent.TYPE_ANNOUNCEMENT).apply {
                text.add(resources.getQuantityString(R.plurals.main_activity_accessibility_results, results, results))
            })
        }
    }
}
