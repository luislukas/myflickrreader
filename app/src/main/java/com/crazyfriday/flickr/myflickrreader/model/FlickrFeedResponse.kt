package com.crazyfriday.flickr.myflickrreader.model

data class FlickrFeedResponse(val title: String, val link: String, val items: Array<FlickrItem>)

