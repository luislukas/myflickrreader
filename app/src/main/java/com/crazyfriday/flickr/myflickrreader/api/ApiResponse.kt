package com.crazyfriday.flickr.myflickrreader.api

interface ApiResponse<T> {
    fun onError()
    fun onNoConnection()
    fun onSuccess(t: T)
}