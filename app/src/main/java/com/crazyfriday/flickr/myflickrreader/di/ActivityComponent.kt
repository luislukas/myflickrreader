package com.crazyfriday.flickr.myflickrreader.di

import com.crazyfriday.flickr.myflickrreader.MainActivity
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = arrayOf(ActivityModule::class, NetworkModule::class))
interface ActivityComponent {
    fun inject(activity: MainActivity)
}