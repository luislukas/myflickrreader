package com.crazyfriday.flickr.myflickrreader.di

import com.crazyfriday.flickr.myflickrreader.api.FlickrFeedResponseResolver
import com.crazyfriday.flickr.myflickrreader.api.ResponseResolver
import com.crazyfriday.flickr.myflickrreader.model.FlickrFeedResponse
import dagger.Module
import dagger.Provides

@Module
class ResponseResolverModule {

    @Provides
    fun provideResponseResolver(): ResponseResolver<FlickrFeedResponse> {
        return FlickrFeedResponseResolver()
    }

}