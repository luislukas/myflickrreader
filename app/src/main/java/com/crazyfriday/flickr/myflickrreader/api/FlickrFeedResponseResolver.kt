package com.crazyfriday.flickr.myflickrreader.api

import retrofit2.Response
import java.io.IOException

class FlickrFeedResponseResolver<T>: ResponseResolver<T> {
    override fun handleResponse(response: Response<T>, handler: ApiResponse<T>) {
        if (response.isSuccessful) {
            handler.onSuccess(response.body())
        } else {
            handler.onError()
        }
    }

    override fun handleErrors(e: Throwable, handler: ApiResponse<T>) {
        if (e is IOException) {
            handler.onNoConnection()
        } else {
            handler.onError()
        }
    }
}