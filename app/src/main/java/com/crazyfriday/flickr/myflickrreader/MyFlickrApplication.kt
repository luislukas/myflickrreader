package com.crazyfriday.flickr.myflickrreader

import android.app.Application
import com.crazyfriday.flickr.myflickrreader.di.Injector

class MyFlickrApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        Injector.init(this)
    }
}