package com.crazyfriday.flickr.myflickrreader.loader

import android.graphics.BitmapFactory
import android.support.media.ExifInterface
import android.text.TextUtils
import android.widget.ImageView
import android.widget.TextView
import okhttp3.*
import java.io.IOException
import java.io.InputStream
import java.lang.ref.WeakReference


class ImageAndMetadataLoader(val mClient: OkHttpClient) {

    private val EDIF_ATTRIBUTES = arrayOf(ExifInterface.TAG_COLOR_SPACE, ExifInterface.TAG_X_RESOLUTION, ExifInterface.TAG_Y_RESOLUTION, ExifInterface.TAG_PIXEL_Y_DIMENSION, ExifInterface.TAG_PIXEL_X_DIMENSION, ExifInterface.TAG_ORIENTATION)

    fun loadImageAndMetadata(weakImage: WeakReference<ImageView>, weakText: WeakReference<TextView>, url: String) {
        val request = Request.Builder().url(url).build()
        mClient.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException?) {
                //NOOP
            }

            override fun onResponse(call: Call, response: Response) {
                val stream = response.body().byteStream()
                val bitmap = BitmapFactory.decodeStream(stream)
                val metadata = extractMetadata(stream)
                weakImage.get()?.post {
                    weakImage.get()?.setImageBitmap(bitmap)
                    weakText.get()?.setText(metadata)
                }
            }
        }
        )
    }

    private fun extractMetadata(inputStream: InputStream): String {
        val sb = StringBuilder()
        try {
            val exitInterface = ExifInterface(inputStream)

            for (property in EDIF_ATTRIBUTES) {
                val value = if (!TextUtils.isEmpty(exitInterface.getAttribute(property))) exitInterface.getAttribute(property) else "N/A"
                sb.append(property.toUpperCase(), " ", value, " ")
            }
        } catch (e: IOException) {
            // NOOP
        } finally {
            inputStream.close()
        }
        return sb.toString()
    }
}