package com.crazyfriday.flickr.myflickrreader.adapter

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.crazyfriday.flickr.myflickrreader.R
import com.crazyfriday.flickr.myflickrreader.loader.ImageAndMetadataLoader
import com.crazyfriday.flickr.myflickrreader.model.FlickrItem
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_layout.*
import java.lang.ref.WeakReference
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.Animation.RELATIVE_TO_SELF
import android.view.animation.ScaleAnimation





class FlickrAdapter(val mList: MutableList<FlickrItem>, val mImageClickCallback: ImageClickCallback, val mImageAndMetadataLoader: ImageAndMetadataLoader) : RecyclerView.Adapter<FlickrAdapter.FlickrViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FlickrViewHolder {
        return FlickrViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_layout, parent, false))
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onBindViewHolder(holder: FlickrViewHolder, position: Int) {
        holder.bindItem(mList.get(position), mImageClickCallback, position, mImageAndMetadataLoader)
    }

    fun updateList(list: List<FlickrItem>) {
        mList.clear()
        mList.addAll(list)
        notifyDataSetChanged()
    }

    class FlickrViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        fun bindItem(item: FlickrItem, imageClickCallback: ImageClickCallback, position: Int, mImageAndMetadataLoader: ImageAndMetadataLoader) {
            containerView.setOnClickListener { imageClickCallback.onImageClick(item) }
            mImage.setImageDrawable(ContextCompat.getDrawable(containerView.context, android.R.drawable.progress_indeterminate_horizontal))
            mTags.text = item.tags
            mImage.contentDescription = item.tags
            mImageMetadata.text = containerView.context.getString(R.string.image_no_metadata)
            mImageAndMetadataLoader.loadImageAndMetadata(WeakReference(mImage), WeakReference(mImageMetadata), item.media.m)
            setScaleAnimation(containerView)
        }
        private fun setFadeAnimation(view: View) {
            val anim = AlphaAnimation(0.0f, 1.0f)
            anim.duration = 1000
            view.startAnimation(anim)
        }

        private fun setScaleAnimation(view: View) {
            val anim = ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
            anim.duration = 1000
            view.startAnimation(anim)
        }
    }

   interface ImageClickCallback {
       fun onImageClick(item: FlickrItem)
   }
}