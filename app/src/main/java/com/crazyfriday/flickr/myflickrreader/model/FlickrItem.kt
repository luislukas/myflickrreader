package com.crazyfriday.flickr.myflickrreader.model


data class FlickrItem(val title: String, val link: String, val tags: String, val media: FlickrMedia)