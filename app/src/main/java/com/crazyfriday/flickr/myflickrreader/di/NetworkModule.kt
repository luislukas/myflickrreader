package com.crazyfriday.flickr.myflickrreader.di

import com.crazyfriday.flickr.myflickrreader.api.Api
import com.crazyfriday.flickr.myflickrreader.api.FlickrApi
import com.crazyfriday.flickr.myflickrreader.api.ResponseResolver
import com.crazyfriday.flickr.myflickrreader.model.FlickrFeedResponse
import dagger.Module
import dagger.Provides

@Module
class NetworkModule {
    @Provides
    fun provideFlickrApi(api: Api, responseResolver: ResponseResolver<FlickrFeedResponse>): FlickrApi {
        return FlickrApi(api, responseResolver)
    }
}