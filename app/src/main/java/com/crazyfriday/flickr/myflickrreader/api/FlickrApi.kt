package com.crazyfriday.flickr.myflickrreader.api

import com.crazyfriday.flickr.myflickrreader.model.FlickrFeedResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import org.jetbrains.annotations.NotNull
import retrofit2.Response

class FlickrApi(private val mApi: Api, private val mResponseResolver: ResponseResolver<FlickrFeedResponse>) {

    private val mCompositeDisposable = CompositeDisposable()

    fun fetch(@NotNull handler: ApiResponse<FlickrFeedResponse>) {
        val disposable = object : DisposableObserver<Response<FlickrFeedResponse>>() {
            override fun onComplete() {
                //NOOP
            }

            override fun onNext(response: Response<FlickrFeedResponse>) {
                mResponseResolver.handleResponse(response, handler)
            }

            override fun onError(e: Throwable) {
                mResponseResolver.handleErrors(e, handler)
            }
        }
        mCompositeDisposable.addAll(disposable)
        mApi.fetchPublicPhotos()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(disposable)
    }

    fun dispose() = mCompositeDisposable.clear()
}