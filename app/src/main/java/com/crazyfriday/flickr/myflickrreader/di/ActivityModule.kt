package com.crazyfriday.flickr.myflickrreader.di

import com.crazyfriday.flickr.myflickrreader.api.FlickrApi
import com.crazyfriday.flickr.myflickrreader.presenter.FlickrPresenter
import dagger.Module
import dagger.Provides

@Module
class ActivityModule {

    @Provides
    @ActivityScope
    fun provideFlickrPresenter(flickrApi: FlickrApi): FlickrPresenter {
        return FlickrPresenter(flickrApi)
    }

}