package com.crazyfriday.flickr.myflickrreader.di

import android.app.Activity
import android.content.Context
import android.support.annotation.VisibleForTesting
import com.crazyfriday.flickr.myflickrreader.MainActivity


class Injector {
    companion object {
        @VisibleForTesting
        lateinit var sAppComponent: AppComponent

        fun init(context: Context) {
            sAppComponent = DaggerAppComponent.builder().appModule(AppModule(context)).build()
        }

        fun get(): AppComponent {
            return sAppComponent
        }

        fun injectActivity(activity: Activity) {
            if (activity is MainActivity) {
                sAppComponent.provideActivityComponent(ActivityModule()).inject(activity)
            }
        }
    }
}