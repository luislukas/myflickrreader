package com.crazyfriday.flickr.myflickrreader.network

import android.content.Context
import android.net.ConnectivityManager

class ConnectivityCheckImpl(val mContext: Context): ConnectivityCheck {

    override fun isConnected(): Boolean {
        val connectivityManager = mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        connectivityManager.activeNetworkInfo?.run {
            return isConnectedOrConnecting
        }
        return false
    }
}