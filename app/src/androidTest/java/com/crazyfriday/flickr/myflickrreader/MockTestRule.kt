package com.crazyfriday.flickr.myflickrreader

import android.app.Activity
import android.support.test.InstrumentationRegistry
import android.support.test.espresso.contrib.AccessibilityChecks
import android.support.test.espresso.intent.rule.IntentsTestRule
import com.crazyfriday.flickr.myflickrreader.di.AppModule
import com.crazyfriday.flickr.myflickrreader.di.DaggerAppComponent
import com.crazyfriday.flickr.myflickrreader.di.Injector
import dagger.Module
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.apache.commons.io.IOUtils
import org.junit.BeforeClass
import java.util.concurrent.TimeUnit

const val HTTP_RESPONSE_SUCCESS = 200
const val HTTP_RESPONSE_ERROR = 404

class MockTestRule<T: Activity>: IntentsTestRule<T> {

    companion object {
        @BeforeClass
        @JvmStatic
        fun enableAccessibility() {
            AccessibilityChecks.enable()
        }
    }

    private lateinit var mMockWebServer: MockWebServer

    constructor(activityClass: Class<T>?, initialTouchMode: Boolean, launchActivity: Boolean) : super(activityClass, initialTouchMode, launchActivity) {
        initialise()
    }

    fun initialise() {
        mMockWebServer = MockWebServer()
        mMockWebServer.start()
        val testModule = TestModule(mMockWebServer.url("/").toString())
        val appComponent = DaggerAppComponent.builder().appModule(testModule).build()
        Injector.sAppComponent = appComponent
    }

    fun shutdown() {
        mMockWebServer.shutdown()
    }

    fun enqueue(body: String) {
        mMockWebServer.enqueue(MockResponse().setResponseCode(HTTP_RESPONSE_SUCCESS).setBody(body))
    }

    fun enqueueError() {
        mMockWebServer.enqueue(MockResponse().setResponseCode(HTTP_RESPONSE_ERROR).setBody("{}"))
    }

    fun enqueueTimeout() {
        mMockWebServer.enqueue(MockResponse().throttleBody(21, 21, TimeUnit.SECONDS))
    }

    fun loadValidResponse(model: String): String {
        val inputStream = javaClass.classLoader.getResourceAsStream(model)
        return IOUtils.toString(inputStream)
    }

    @Module
    class TestModule(private val mURL: String): AppModule(InstrumentationRegistry.getTargetContext()) {
        override fun getBaseURL(): String {
            return mURL
        }
    }
}