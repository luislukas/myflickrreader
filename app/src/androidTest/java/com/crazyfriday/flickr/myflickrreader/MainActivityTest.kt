package com.crazyfriday.flickr.myflickrreader

import android.app.Instrumentation
import android.content.Intent
import android.net.Uri
import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.action.ViewActions.swipeDown
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import android.support.test.espresso.intent.Intents.intending
import android.support.test.espresso.intent.matcher.IntentMatchers
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.runner.AndroidJUnit4
import com.crazyfriday.flickr.myflickrreader.adapter.FlickrAdapter
import org.hamcrest.CoreMatchers.*
import org.junit.After
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class MainActivityTest {
    private val MODEL = "valid_response.json"
    private val TAGS = "ifttt instagram"
    private val MODEL_REFRESHED = "valid_response_refreshed.json"
    private val TAGS_REFRESHED = "ifttt instagram refreshed"
    private val TEST_URL = "https://farm5.staticflickr.com/4724/25294458297_c1f50d152e_m.jpg"

    @Rule
    @JvmField
    val mTestRule = MockTestRule<MainActivity>(MainActivity::class.java, true, false)

    @After
    fun tearDown() {
        mTestRule.shutdown()
    }

    @Test
    fun onTimeoutDisplaysNoConnection() {
        mTestRule.enqueueTimeout()
        mTestRule.launchActivity(null)
        InstrumentationRegistry.getInstrumentation().waitForIdleSync()
        onView(withText(containsString(mTestRule.activity.getString(R.string.api_on_no_connection_txt)))).check(matches(isDisplayed()))
        onView(withId(R.id.mRecyclerView)).check(matches(not(isDisplayed())))
    }

    @Test
    fun onErrorDisplaysErrorMessage() {
        mTestRule.enqueueError()
        mTestRule.launchActivity(null)
        onView(withText(containsString(mTestRule.activity.getString(R.string.api_on_error_txt)))).check(matches(isDisplayed()))
        onView(withId(R.id.mRecyclerView)).check(matches(not(isDisplayed())))
    }

    @Test
    fun displaysImagesAndTagsCorrectlyAfterRefresh() {
        mTestRule.enqueue(mTestRule.loadValidResponse(MODEL))
        mTestRule.launchActivity(null)

        onView(withId(R.id.mRecyclerView)).check(matches(isDisplayed()))
        onView(withId(R.id.mErrorContainer)).check(matches(not(isDisplayed())))
        onView(withId(R.id.mRecyclerView)).check(matches(hasDescendant(withText(TAGS))))

        mTestRule.enqueue(mTestRule.loadValidResponse(MODEL_REFRESHED))
        onView(withId(R.id.mRecyclerView)).perform(swipeDown())
        onView(withId(R.id.mRecyclerView)).check(matches(hasDescendant(withText(TAGS_REFRESHED))))

        val expected = allOf(
                IntentMatchers.hasAction(Intent.ACTION_VIEW),
                IntentMatchers.hasData(Uri.parse(TEST_URL))
        )
        intending(expected).respondWith(Instrumentation.ActivityResult(0, null))
        onView(withId(R.id.mRecyclerView)).perform(actionOnItemAtPosition<FlickrAdapter.FlickrViewHolder>(0, click()))
    }
}