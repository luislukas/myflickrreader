package com.crazyfriday.flickr.myflickrreader

import com.crazyfriday.flickr.myflickrreader.api.ApiResponse
import com.crazyfriday.flickr.myflickrreader.api.FlickrApi
import com.crazyfriday.flickr.myflickrreader.model.FlickrFeedResponse
import com.crazyfriday.flickr.myflickrreader.model.FlickrItem
import com.crazyfriday.flickr.myflickrreader.presenter.FlickrMvp
import com.crazyfriday.flickr.myflickrreader.presenter.FlickrPresenter
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

class FlickrPresenterTest {
    private lateinit var SUT: FlickrPresenter
    private val mFlickrApi = mock(FlickrApi::class.java)
    private val mView = mock(FlickrMvp.View::class.java)
    private val mResponse = mock(FlickrFeedResponse::class.java)

    @Captor
    private lateinit var mCaptor: ArgumentCaptor<ApiResponse<FlickrFeedResponse>>

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        SUT = FlickrPresenter(mFlickrApi)
    }

    @Test
    fun onSuccessfulResponse() {
        val results = arrayOf(mock(FlickrItem::class.java))
        `when`(mResponse.items).thenReturn(results)

        SUT.onAttachView(mView)

        verify(mView, times(1)).displayProgress(true)
        verify(mFlickrApi, times(1)).fetch(capture(mCaptor))

        mCaptor.value.onSuccess(mResponse)

        verify(mView, times(1)).displayProgress(false)
        verify(mView, times(1)).dismissSwipeToRefresh()
        verify(mView, times(1)).onSuccess(mResponse)
        verify(mView, times(1)).announceResults(results.size)
        verify(mView, never()).onNoConnection()
        verify(mView, never()).onError()
    }

    @Test
    fun onSuccessfulCachedResponse() {
        val results = arrayOf(mock(FlickrItem::class.java))
        `when`(mResponse.items).thenReturn(results)

        SUT.onAttachView(mView)

        verify(mFlickrApi, times(1)).fetch(capture(mCaptor))

        mCaptor.value.onSuccess(mResponse)

        SUT.onDetachView()

        SUT.onAttachView(mView)

        verify(mView, times(2)).displayProgress(true)
        verify(mView, times(2)).displayProgress(false)
        verify(mView, times(2)).dismissSwipeToRefresh()
        verify(mView, times(2)).onSuccess(mResponse)
        verify(mView, times(2)).announceResults(results.size)
        verify(mFlickrApi, never()).dispose()
        verify(mView, never()).onNoConnection()
        verify(mView, never()).onError()
    }

    @Test
    fun onRequestError() {
        SUT.onAttachView(mView)

        verify(mView, times(1)).displayProgress(true)
        verify(mFlickrApi, times(1)).fetch(capture(mCaptor))

        mCaptor.value.onError()

        verify(mView, times(1)).displayProgress(false)
        verify(mView, times(1)).dismissSwipeToRefresh()
        verify(mView, times(1)).onError()
        verify(mView, never()).onSuccess(mResponse)
        verify(mView, never()).onNoConnection()
    }

    @Test
    fun onNoConnection() {
        SUT.onAttachView(mView)

        verify(mView, times(1)).displayProgress(true)
        verify(mFlickrApi, times(1)).fetch(capture(mCaptor))

        mCaptor.value.onNoConnection()

        verify(mView, times(1)).displayProgress(false)
        verify(mView, times(1)).dismissSwipeToRefresh()
        verify(mView, times(1)).onNoConnection()
        verify(mView, never()).onSuccess(mResponse)
        verify(mView, never()).onError()
    }

    @Test
    fun onDestroyDisposesRequests() {
        SUT.onDestroy()
        verify(mFlickrApi, times(1)).dispose()
    }

}