package com.crazyfriday.flickr.myflickrreader.api

import com.crazyfriday.flickr.myflickrreader.anyType
import com.crazyfriday.flickr.myflickrreader.model.FlickrFeedResponse
import io.reactivex.Observable
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.never
import org.mockito.Mockito.times
import retrofit2.Response
import java.net.SocketTimeoutException

@Suppress("UNCHECKED_CAST")
class FlickrApiTest {
    private lateinit var SUT: FlickrApi

    private val mApi = Mockito.mock(Api::class.java)
    private val mResponseResolver : ResponseResolver<FlickrFeedResponse> = Mockito.mock(ResponseResolver::class.java) as ResponseResolver<FlickrFeedResponse>

    @Rule
    @JvmField
    val mOverrideSchedulersRule = RxSchedulersOverrideRule()

    @Before
    fun setup() {
        SUT = FlickrApi(mApi, mResponseResolver)
    }

    @Test
    fun onSuccess() {
        val responseHandler : ApiResponse<FlickrFeedResponse> = Mockito.mock(ApiResponse::class.java) as ApiResponse<FlickrFeedResponse>
        val response = Mockito.mock(FlickrFeedResponse::class.java)
        val mockObservableData : Observable<Response<FlickrFeedResponse>> = Observable.just(Response.success(response))

        Mockito.`when`(mApi.fetchPublicPhotos()).thenReturn(mockObservableData)

        SUT.fetch(responseHandler)

        Mockito.verify(mResponseResolver, times(1)).handleResponse(anyType(), anyType())
    }

    @Test
    fun onError() {
        val responseHandler : ApiResponse<FlickrFeedResponse> = Mockito.mock(ApiResponse::class.java) as ApiResponse<FlickrFeedResponse>
        val exception : SocketTimeoutException = Mockito.mock(SocketTimeoutException::class.java)

        Mockito.`when`(mApi.fetchPublicPhotos()).thenReturn(Observable.error(exception))

        SUT.fetch(responseHandler)

        Mockito.verify(mResponseResolver, times(1)).handleErrors(anyType(), anyType())
        Mockito.verify(mResponseResolver, never()).handleResponse(anyType(), anyType())
    }
}