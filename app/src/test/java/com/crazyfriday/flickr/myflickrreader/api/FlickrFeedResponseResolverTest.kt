package com.crazyfriday.flickr.myflickrreader.api

import com.crazyfriday.flickr.myflickrreader.anyType
import com.crazyfriday.flickr.myflickrreader.model.FlickrFeedResponse
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.*
import retrofit2.Response
import java.io.IOException

@Suppress("UNCHECKED_CAST")
class FlickrFeedResponseResolverTest {
    private lateinit var SUT: ResponseResolver<FlickrFeedResponse>

    private val mHandler : ApiResponse<FlickrFeedResponse> = Mockito.mock(ApiResponse::class.java) as ApiResponse<FlickrFeedResponse>
    private val mResponse = mock(Response::class.java) as Response<FlickrFeedResponse>

    @Before
    fun setup() {
        SUT = FlickrFeedResponseResolver()
    }

    @Test
    fun onResponseSuccess() {
        val body = Mockito.mock(FlickrFeedResponse::class.java)

        Mockito.`when`(mResponse.isSuccessful).thenReturn(true)
        Mockito.`when`(mResponse.body()).thenReturn(body)

        SUT.handleResponse(mResponse, mHandler)

        verify(mHandler, times(1)).onSuccess(body)
        verify(mHandler, never()).onError()
        verify(mHandler, never()).onNoConnection()
    }

    @Test
    fun onError() {
        val body = Mockito.mock(FlickrFeedResponse::class.java)

        Mockito.`when`(mResponse.isSuccessful).thenReturn(false)

        SUT.handleResponse(mResponse, mHandler)

        verify(mHandler, times(1)).onError()
        verify(mHandler, never()).onSuccess(body)
        verify(mHandler, never()).onNoConnection()
    }

    @Test
    fun onNoConnection() {
        val error = mock(IOException::class.java)

        SUT.handleErrors(error, mHandler)

        verify(mHandler, times(1)).onNoConnection()
        verify(mHandler, never()).onSuccess(anyType())
        verify(mHandler, never()).onError()
    }

    @Test
    fun onOtherException() {
        val error = mock(Exception::class.java)

        SUT.handleErrors(error, mHandler)

        verify(mHandler, times(1)).onError()
        verify(mHandler, never()).onSuccess(anyType())
        verify(mHandler, never()).onNoConnection()
    }
}